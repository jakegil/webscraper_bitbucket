import bs4 as bs
import urllib.request
import os,sys
import re
import pandas as pd
import nltk

|#story string of arrays for each parapraph
def NLP_Story(story):




    return



def Get_story(link:string):
    sauce = urllib.request.urlopen(link).read()
    #change to what code is being used e.g html
    soup = bs.BeautifulSoup(sauce, 'lxml')
    body = soup.body
    articleStory = []

    for paragraph in body.find_all("p"):
        articleStory.append(paragraph.text)

    return articleStory


#Remove company liability status
def ASX_Listed_Companies():
    #Retrieve Spreadsheet
    ASX_Listed_Companies = r'C:\Users\TechFast Australia\Desktop\Code\Python\Finance\Webscraper\ASXListedCompanies.csv'
    DF_ASX_Listed_Companies = pd.read_csv(ASX_Listed_Companies)

    #Clean up Spreadsheet
    companyClassifications = [' LIMITED',' LTD', ' INC',' PLC',' REIT', ' TRUST', 'GROUP']
    for no,company in DF_ASX_Listed_Companies['Company name'].items():
        for companyStatus in companyClassifications:
            if companyStatus in company:
                DF_ASX_Listed_Companies['Company name'][no] = DF_ASX_Listed_Companies['Company name'][no].replace(companyStatus, '')
                
    return DF_ASX_Listed_Companies

def Ticker_pull (Articles):
    new_Articles = []
    DF_ASX_Listed_Companies = ASX_Listed_Companies()
    for article in Articles:
        for no, company in DF_ASX_Listed_Companies['Company name'].items():
            if company.lower() in article.title.lower():
                # print(article.title)
                # print("Found company in Article")
                # print(company)
                # print(no)
                article.ticker = DF_ASX_Listed_Companies['ASX code'][no]
                article.company = company
                ### Add functionality to grab stock price
                break
        new_Articles.append(article)
    return new_Articles
class Article:
    def __init__(self,title,date,link,ticker = 'XXX',company = '?',spotPrice = '$0', story):
        self.title = title
        self.date = date
        self.link = link
        self.ticker = ticker
        self.company = company
        self.spotPrice = spotPrice
        self.story = story
    
    def print_values(self):
        print(self.title)
        print(self.date)
        print(self.ticker)
        print(self.spotPrice)

sauce = urllib.request.urlopen('https://smallcaps.com.au/').read()
#change to what code is being used e.g html
soup = bs.BeautifulSoup(sauce, 'lxml')
body = soup.body
Articles = []

for class_item in body.find_all("div", {"class":"item-details"}):
    title = class_item.find('a').text
    date = class_item.find('time').text
    link = class_item.get('href')
    # function that pulls the ticker

    # function that pulls the price 


    dataClass = Article(title,date,link)
    Articles.append(dataClass)
    Articles = Ticker_pull(Articles)
# Create a Dataframe 
#Intialise
df = {
        'title':[],
        'date':[],
        'ticker':[],
        'company':[],
        'spotPrice':[]
}
for data in Articles:
    df['title'].append(data.title)
    df['date'].append(data.date)
    df['ticker'].append(data.ticker)
    df['company'].append(data.company)
    df['spotPrice'].append(data.spotPrice)

df = pd.DataFrame(df)
df.to_csv(r'C:\Users\TechFast Australia\Desktop\Code\Python\Finance\Webscraper\smallCapData.csv')